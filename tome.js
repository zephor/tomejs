APIer = (function() {
    var ARGUMENTS_REGEX = /\(([\s\S]*?)\)/;
    var NEWLINE_REGEX = /\r?\n|\r/g;
    var SPACES_REGEX = /\s/g;
    var RETURN_VALUES_REGEX = /return.*(?=;)/g;
    var LEADING_WHITESPACE_REGEX = /^\s*/g;
    var TRAILING_WHITESPACE_REGEX = /\s*$/g;

    var APIer = function(API) {
        var result = [];

        for(i in API) {
            result[i] = getAPIPoint(API[i]);
        }
        console.log(result);
        return result;
    }

    return APIer;

    function getAPIPoint(property) {
        var result = {};
        result.APIType = typeof property;
        if(typeof property === 'function') {
            arguments = extractFunctionParameters(property);
            if(arguments.length > 0) {
                result.parameters = arguments;
            }
            result.returnValues = extractReturnValues(property);
        } else {
            result.propertyValue = property
        }
        return result;
    }

    function extractFunctionParameters(func) {
        var argumentsSection = func.toString().match(ARGUMENTS_REGEX)[1];
        var arguments = argumentsSection.replace(NEWLINE_REGEX, '')
            .replace(SPACES_REGEX, '')
            .split(',');

        return arguments[0] !== '' ? arguments : false;
    }

    function extractReturnValues(func) {
        //TODO : detect line breaks and brackets
        var returnLines = func.toString().match(RETURN_VALUES_REGEX);
        var returnValues = [];
        for(i in returnLines) {
            returnValues.push(trimString(returnLines[i].replace('return', '')));
        }
        return returnValues;
    }

    function trimString(string) {
        return string.replace(LEADING_WHITESPACE_REGEX, '').replace(TRAILING_WHITESPACE_REGEX, '');
    }
})()

